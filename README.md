# Open Street Map · Natural Solutions

<div align="center">
<center>
    <img alt="pipeline" src="https://gitlab.com/natural-solutions/osm/badges/master/pipeline.svg" />
    <img alt="license" src="https://img.shields.io/npm/l/osm" />
</center>
</div>

<br />

## Local development

```bash
docker-compose up -d
```

- Traefik UI: http://localhost:8881
- Tiles url: http://localhost:8880/tiles
- API Docs: http://localhost:8880/api/docs
- Meilisearch: http://localhost:7700

## Production

- Tiles url: https://osm.natural-solutions.eu/tiles
- API Docs: https://osm.natural-solutions.eu/api/docs
- Meilisearch: https://osm.natural-solutions.eu/meilisearch
