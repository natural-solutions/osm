#!/bin/sh

TAG=0.11.1

curl -L -o imposm3.tar.gz https://github.com/omniscale/imposm3/releases/download/v${TAG}/imposm-${TAG}-linux-x86-64.tar.gz
mkdir /opt/imposm
tar -xf imposm3.tar.gz --strip 1 -C /opt/imposm 