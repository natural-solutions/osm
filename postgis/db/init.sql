CREATE DATABASE osm;
\c osm;
CREATE EXTENSION postgis;
CREATE EXTENSION hstore;

CREATE TABLE public.osm_trees (
	id serial NOT NULL,
	osm_id int8 NOT NULL,
	"name" varchar NULL,
	sex varchar NULL,
	height int4 NULL,
	diameter int4 NULL,
	protected bool NULL,
	"type" varchar NULL,
	geometry geometry(POINT, 4326) NULL,
	CONSTRAINT osm_trees_pkey PRIMARY KEY (osm_id, id)
);

CREATE INDEX osm_trees_geom ON public.osm_trees USING gist (geometry);

COPY public.osm_trees FROM '/tmp/osm_trees.csv' DELIMITER ',' CSV HEADER;