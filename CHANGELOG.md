# [1.3.0](https://gitlab.com/natural-solutions/osm/compare/v1.2.1...v1.3.0) (2021-05-07)


### Features

* add meilisearch to project ([ad739ad](https://gitlab.com/natural-solutions/osm/commit/ad739ad2195f8ca98b254286bcd88fdf536e566a))

## [1.2.1](https://gitlab.com/natural-solutions/osm/compare/v1.2.0...v1.2.1) (2021-05-06)


### Bug Fixes

* replace GENERATE_VECTOR_TILES_CONNECTION ([361fb11](https://gitlab.com/natural-solutions/osm/commit/361fb1196e31c94a1f7dc8f445b4c952e07c5fda))

# [1.2.0](https://gitlab.com/natural-solutions/osm/compare/v1.1.1...v1.2.0) (2021-05-06)


### Features

* add lets encrypt ([f5da344](https://gitlab.com/natural-solutions/osm/commit/f5da344a59f830444e604b4e19ac8b8ff64f02c6))

## [1.1.1](https://gitlab.com/natural-solutions/osm/compare/v1.1.0...v1.1.1) (2021-05-04)


### Bug Fixes

* build api ([5f69689](https://gitlab.com/natural-solutions/osm/commit/5f69689d6c68a8ebb17d72d1a2af1a742dfb5de6))

# [1.1.0](https://gitlab.com/natural-solutions/osm/compare/v1.0.0...v1.1.0) (2021-05-04)


### Features

* add CHANGELOG.md ([e1a4e43](https://gitlab.com/natural-solutions/osm/commit/e1a4e43c25b6eeaffd00778a22ad0864fe838e87))
