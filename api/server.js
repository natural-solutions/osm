const fastify = require("fastify")({ logger: true });
const port = process.env.SERVER_PORT || 3000;
const host = process.env.HOST || "http://localhost:3000";
const schemas = process.env.SCHEMAS || ["http"];
const apiVersion = "/api/v1";

fastify
  .register(require("fastify-swagger"), {
    routePrefix: `${apiVersion}/docs`,
    swagger: {
      info: {
        title: "OSM API · Natural Solutions ",
        description: "API Overview",
        version: "0.1.0",
      },
      host,
      schemas,
      consumes: ["application/json"],
      produces: ["application/json"],
    },
    exposeRoute: true,
  })
  .register(require("./routes/thumbnail"), { prefix: apiVersion });

const start = async () => {
  try {
    await fastify.listen(port, "0.0.0.0");
  } catch (err) {
    fastify.log.error(err);
    process.exit(1);
  }
};

start();
