const puppeteer = require("puppeteer");
const osmGeoJson = require("osm-geojson");
const fs = require("fs");

async function thumbnail({
  osmid,
  height,
  width,
  filename,
  padding = 20,
  timeout = 4000,
}) {
  const geojson = await osmGeoJson.get(osmid);
  const browser = await puppeteer.launch({
    defaultViewport: null,
    headless: true,
    args: ["--no-sandbox", "--disable-setuid-sandbox"],
  });
  const page = await browser.newPage();
  await page.setViewport({ width, height });
  await page.goto(`file://${process.cwd()}/index.html`);
  await page.evaluate(
    (data) => {
      loadLayers(data);
    },
    { geojson, padding }
  );
  await page.waitForTimeout(timeout);
  await page.screenshot({ path: filename, fullPage: true });
  await browser.close();
}

module.exports = function (fastify, opts, next) {
  fastify.get(
    "/thumbnail/:osmid",
    {
      schema: {
        description: "Generate thumbnail based from osm_id",
        tags: ["thumbnail"],
        summary: "",
        params: {
          osmid: { type: "string" },
        },
        query: {
          noCache: { type: "boolean", default: false },
          width: { type: "number", default: 100 },
          height: { type: "number", default: 100 },
          timeout: { type: "number", default: 4000 },
        },
      },
    },
    async (request, reply) => {
      const { osmid } = request.params;
      const { height, width, noCache, timeout } = request.query;
      const filename = `./data/${osmid}_${height}_${width}.png`;
      const params = { osmid, height, width, filename, timeout };

      try {
        const exists = await fs.promises.access(filename);

        if (!exists && noCache) {
          await thumbnail(params);
        }
      } catch (e) {
        await thumbnail(params);
      }

      const stream = fs.createReadStream(filename);
      reply.type("image/png").send(stream);
    }
  );

  next();
};
